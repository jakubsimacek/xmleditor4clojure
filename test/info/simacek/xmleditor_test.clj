(ns info.simacek.xmleditor-test
  (:require [clojure.test :refer :all]
            [info.simacek.xmleditor :refer :all]
            [clojure.java.io :as io]))

(defn with-example-1 [f]
  (let [xdoc (load-xml (io/input-stream "test/resources/example1.xml"))]
    (f xdoc)))


(deftest get-node-str-1
  (testing "Simplest string element retrieval"
    (with-example-1
      (fn [xdoc]
        [(is (= "Nazdar"
                (get-node-str xdoc "/root/ele")))
         (is (= "val2"
                (get-node-str xdoc "/root/parent/child[2]")))
         (is (= "3"
                (get-node-str xdoc "count(/root/parent/child)")))]))))


(deftest set-node-value-1
  (testing "Simplest string element setting"
    (with-example-1
      (fn [xdoc]
        (set-text-node-value xdoc "/root/ele/text()" {} "Salute")
        [(is (= "Salute"
                (get-node-str xdoc "/root/ele")))]))))


(deftest remove-node-str-1
  (testing "Removing element"
    (with-example-1
      (fn [xdoc]
        (remove-nodes xdoc "/root/ele")
        [(is (= "false"
                (get-node-str xdoc "count(/root/ele) > 0")))]))))

(deftest remove-node-str-saxon-1
  (testing "Removing element and testing using XPath 2.0 exists function"
    (with-example-1
      (fn [xdoc]
        (remove-nodes xdoc "/root/ele")
        [(is (= "false"
                (get-node-str xdoc "exists(/root/ele)")))]))))

;(remove-nodes xdoc "/root/ele" {})
;(remove-nodes xdoc "/root/parent/child[position() > 1]" {})

(defn xf1 [old new]
  (str "old: " old ", new: " new))

(deftest set-node-value-by-func1
  (testing "Simplest string element setting"
    (with-example-1
      (fn [xdoc]
        (set-text-node-value-by-func xdoc "/root/ele/text()" {} xf1 "Salute")
        [(is (= "old: Nazdar, new: Salute"
                (get-node-str xdoc "/root/ele")))]))))

(deftest print-test-1
  (testing "Print test"
    (with-example-1
      (fn [xdoc]
        (let [os (io/output-stream "/junkyard/out.xml")]
        (save-xml xdoc os)
        [(is (= "x"
                "xy"))])))))
