(defproject xmleditor4clojure "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 #_ [net.sf.saxon/saxon-xpath "9.x"]
                 #_ [net.sf.saxon/saxon "8.7"]
                 [net.sf.saxon/Saxon-HE "9.9.1-1"]]
  :repl-options {:init-ns xmleditor4clojure.core})
