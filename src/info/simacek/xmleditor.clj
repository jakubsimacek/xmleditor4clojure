(ns info.simacek.xmleditor
  #_ (:require [clojure.java.io :as io]))

(import javax.xml.parsers.DocumentBuilderFactory)
(import javax.xml.xpath.XPathFactory)
(import javax.xml.xpath.XPathConstants)
(import javax.xml.transform.TransformerFactory)
(import javax.xml.transform.OutputKeys)
(import javax.xml.transform.dom.DOMSource)
(import javax.xml.transform.stream.StreamResult)
(import net.sf.saxon.xpath.XPathFactoryImpl)

(defn load-xml
  "Loads an org.w3c.dom.Document from java.io.InputStream and returns it
  along with other objects needed for XML processing."
  [input-stream]
  (let [factory (DocumentBuilderFactory/newInstance)
        builder (.newDocumentBuilder factory)
        xpath-factory (XPathFactoryImpl.)
        xpath (.newXPath xpath-factory)
        doc (.parse builder input-stream)]
    {:factory factory,
     :builder builder,
     :xpath-factory xpath-factory
     :xpath xpath
     :xroot doc}))


(defn get-node
  "Returns a node value using XPath and optionally namespace map."
  ([ret-type xdoc xpath-str]
   (get-node ret-type xdoc xpath-str {}))

  ([ret-type xdoc xpath-str namespace-map]
   (let [xpath (:xpath xdoc)
         xroot (:xroot xdoc)
         int-ret-type (case ret-type
                        :string XPathConstants/STRING
                        :node XPathConstants/NODE
                        :node-set XPathConstants/NODESET
                        ret-type)
         nss (reify javax.xml.namespace.NamespaceContext
               (getNamespaceURI [this prefix]
                 (println (str "::" namespace-map prefix))
                 (get namespace-map prefix))

               (getPrefix [this ns-uri]
                 (println "rrrrr")
                 #_(get namespaces-inv ns-uri))

               (getPrefixes [this ns-uri]
                 (println "xxxxxx")
                 ("")))
         xexpr (.compile xpath xpath-str)]
     (.setNamespaceContext xpath nss)
     (.evaluate xexpr xroot int-ret-type))))

(defn get-node-str
   "Returns a node value using XPath"
  ([xdoc xpath-str]
   (get-node :string xdoc xpath-str))
  ([xdoc xpath-str namespace-map]
   (get-node :string xdoc xpath-str namespace-map)))

(defn get-node-set
   "Returns a node set using XPath"
  ([xdoc xpath-str]
   (get-node :node-set xdoc xpath-str))
  ([xdoc xpath-str namespace-map]
   (get-node :node-set xdoc xpath-str namespace-map)))




(defn remove-nodes
  "Remove nodes from XML document selected using an XPath expression"
  ([xdoc xpath-str]
   (remove-nodes xdoc xpath-str {}))
  ([xdoc xpath-str namespace-map]
   (let [xpath (:xpath xdoc)
        xroot (:xroot xdoc)
        xnode-set (get-node-set xdoc xpath-str namespace-map)]
     (let [cnt (.getLength xnode-set)]
       (loop [idx 0]
         (if (= idx cnt)
           xroot
           (let [nod (.item xnode-set idx)
                 _ (println idx)
                 _ (println nod)]
                 (->
                  (.getParentNode nod)
                  (.removeChild nod))
                 (recur (inc idx)))
           ))))))


         
;(def x-path (:xpath xdoc))
;(def x-root (:xroot xdoc))
;(def x-node-set (get-node-set xdoc "/root/ele" {}))
;(def x-cnt (.getLength x-node-set))
;(def x-nod (.item x-node-set 0))
;(def x-parent (.getParentNode x-nod))
;(def x-removed (.removeChild x-parent x-nod))
;(:xroot xdoc)

(defn save-xml
  ([xnode output-stream]
   (save-xml xnode output-stream false))

  ([xnode output-stream omit-preambule]
   (let [transf-factory (TransformerFactory/newInstance)
         _ (println transf-factory)
         transf (.newTransformer transf-factory)]
     (.setOutputProperty transf OutputKeys/OMIT_XML_DECLARATION (if (true? omit-preambule) "yes" "no"))
     #_(.setOutputProperty transf OutputKeys/INDENT "yes")
     #_(.setOutputProperty transf "{http://xml.apache.org/xslt}indent-amount" "4")
     (let [source (DOMSource. (:xroot xnode))
           result (StreamResult. output-stream)]
       (.transform transf source result)
       ))))

(defn set-text-node-value [xml xpath-str namespace-map value]
  (let [xpath (:xpath xml)
        xroot (:xroot xml)
        xnode (get-node :node xml xpath-str namespace-map)]
    (if-not (nil? xnode)
      (.setNodeValue xnode value))))
    
(defn set-text-node-value-by-func [xml xpath-str namespace-map f value]
  (let [xpath (:xpath xml)
        xroot (:xroot xml)
        xnode (get-node :node xml xpath-str namespace-map)]
    (if-not (nil? xnode)
      (.setNodeValue xnode (f (.getTextContent xnode) value)))))


;(def xdoc (load-xml (io/input-stream "test/resources/example1.xml")))


;(get-node-str xdoc "/ns:root" namespaces)

;(get-node-str xdoc "/root/ele")
;(get-node-str xdoc "count(/root/parent/child)")

;(get-node-str xdoc "/root/otherns")

;(get namespaces "ns")


;(set-text-node-value xdoc "/root/ele/text()" {} "salute")


;(print-document xdoc *out* true)
;(def namespaces {"ns" "nam"
;                 "oth" "othernamespace"})

;(def namespaces-inv (clojure.set/map-invert namespaces))


